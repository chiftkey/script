#!/bin/sh

REPONAME="cheat/cheat"
PRGNAME="cheat"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
#VERSION_NAME=`echo ${VERSION} | cut -c 2- `
BINNAME="${PRGNAME}-linux-amd64"
GZIP_FILENAME="${BINNAME}.gz"
#TAR_FILENAME="${BINNAME}-${VERSION_NAME}.x86_64.tar.gz"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${GZIP_FILENAME}
echo "${DOWNLOAD_URL}"

# download package
curl -LO --silent ${DOWNLOAD_URL}
sleep 1
#tar -zxvf ${TAR_FILENAME}
gzip -d ${GZIP_FILENAME}
install ${BINNAME} ~/.bin/${PRGNAME}
# delete deb file
rm -f ${GZIP_FILENAME} ${BINNAME}
