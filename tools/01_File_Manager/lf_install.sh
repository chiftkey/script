#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

#https://github.com/gokcehan/lf/releases/download/r24/lf-linux-amd64.tar.gz

REPONAME="gokcehan/lf"
PRGNAME="lf"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
#VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="tar.gz"
FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH2}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
tar zxf ${FILENAME}
# install file
install ${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp

# https://github.com/gokcehan/lf/wiki/Tutorial#configuration
LF_CONF_DIR=${HOME}/.config/lf
mkdir -p ${LF_CONF_DIR}
curl https://raw.githubusercontent.com/gokcehan/lf/master/etc/lfrc.example -o ${LF_CONF_DIR}/lfrc
