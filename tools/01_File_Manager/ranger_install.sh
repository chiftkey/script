#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

if [ -z "$(command -v pip3)" ]; then
    if [ "${UBUNTU_CODENAME}" = "xenial" ]; then
	curl https://bootstrap.pypa.io/pip/3.5/get-pip.py | python3
    else
    	sudo apt install python3-pip -y
    fi
fi

if [ -z "$(command -v ranger)" ]; then
    pip3 install ranger-fm
else
    pip3 install -U ranger-fm
fi
