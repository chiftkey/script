#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

#https://github.com/gokcehan/lf/releases/download/r24/lf-linux-amd64.tar.gz
# https://github.com/Canop/broot/releases/download/v1.7.5/broot_1.7.5.zip

REPONAME="Canop/broot"
PRGNAME="broot"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="zip"
# FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH2}.${FILENAME_EXTENSION}"
FILENAME="${PRGNAME}_${VERSION_NAME}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
unzip ${FILENAME}
# install file
install ./build/${SYSTEM_ARCH}-${KERNEL_NAME_LOWERCASE}/${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp
