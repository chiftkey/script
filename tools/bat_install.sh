#!/bin/sh

# https://github.com/sharkdp/bat/releases/download/v0.18.3/bat_0.18.3_amd64.deb

REPONAME="sharkdp/bat"
PRGNAME="bat"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME="${PRGNAME}_${VERSION_NAME}_amd64.deb"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# install
sudo apt install ./${FILENAME}
# delete files
cd .. && rm -rf ./tmp
