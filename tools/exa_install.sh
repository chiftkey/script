#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# A modern replacement for ‘ls’.
# https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip

REPONAME="ogham/exa"
PRGNAME="exa"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
# FILENAME_SUFFIX="musl"
FILENAME_SUFFIX=""
FILENAME_EXTENSION="zip"
# FILENAME="${PRGNAME}-${VERSION_NAME}-${SYSTEM_ARCH}-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH}-${VERSION}.${FILENAME_EXTENSION}"
echo "FileName = ${FILENAME}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
unzip ${FILENAME}
# install
install ./bin/${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp
