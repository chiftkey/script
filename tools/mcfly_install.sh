#!/bin/sh

# McFly - fly through your shell history
# Shell history finder

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# https://github.com/cantino/mcfly/releases/download/v0.5.10/mcfly-v0.5.10-x86_64-unknown-linux-musl.tar.gz

REPONAME="cantino/mcfly"
PRGNAME="mcfly"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_SUFFIX="musl"
FILENAME_EXTENSION="tar.gz"
# FILENAME="${PRGNAME}-${VERSION_NAME}-${SYSTEM_ARCH}-unknown-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
FILENAME="${PRGNAME}-${VERSION}-${SYSTEM_ARCH}-unknown-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
echo "FileName = ${FILENAME}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
tar xf ${FILENAME}
# install file
install ${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp
