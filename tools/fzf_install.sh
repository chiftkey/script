#!/bin/sh

if [ -d ~/.fzf ]; then
	echo "Update fzf"
	cd ~/.fzf && git pull
else
	git clone https://github.com/junegunn/fzf.git ~/.fzf
fi

~/.fzf/install --key-bindings --completion --no-update-rc
