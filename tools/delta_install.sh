#!/bin/sh

REPONAME="dandavison/delta"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
FILENAME="git-delta_${VERSION}_amd64.deb"

# download package
curl -LO https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
sleep 1
# install downloaded package
sudo apt install ./${FILENAME}
# delete deb file
rm -f ${FILENAME}
