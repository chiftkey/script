#!/bin/sh

# Log Viewer
# https://github.com/tstack/lnav/releases/download/v0.10.1/lnav-0.10.1-musl-64bit.zip

REPONAME="tstack/lnav"
PRGNAME="lnav"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME="${PRGNAME}-${VERSION_NAME}-musl-64bit.zip"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
unzip ${FILENAME}
cd ${PRGNAME}-${VERSION_NAME}
# install
chmod a+x ${PRGNAME}
# move home directory
mv ${PRGNAME} ${HOME}/.bin/
# delete files
cd ../../ && rm -rf ./tmp
