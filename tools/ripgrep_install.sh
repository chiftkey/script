#!/bin/sh

# ripgrep
# ripgrep is a line-oriented search tool that recursively searches the current directory for a regex pattern

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

REPONAME="BurntSushi/ripgrep"
PRGNAME="${REPONAME#*/}"
BINNAME="$(echo ${PRGNAME} | cut -c 1,4)"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_SUFFIX="musl"
FILENAME_EXTENSION="tar.gz"
# FILENAME="${PRGNAME}-${VERSION_NAME}-${SYSTEM_ARCH}-unknown-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
FILENAME="${PRGNAME}-${VERSION}-${SYSTEM_ARCH}-unknown-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}"
FULL_FILENAME="${FILENAME}.${FILENAME_EXTENSION}"
echo "FileName = ${FULL_FILENAME}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FULL_FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
tar xf ${FULL_FILENAME}
# install binary
install -m 755 -v ${FILENAME}/${BINNAME} ${USER_LOCAL_BIN_DIR}/
# delete files
cd .. && rm -rf ./tmp
