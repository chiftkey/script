#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

printInstallMessage "LibreOffice"
sudo apt install libreoffice-impress libreoffice-calc libreoffice-writer
