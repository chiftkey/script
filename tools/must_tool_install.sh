#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

if [ -z "$(command -v curl)" ]; then
	printInstallMessage "curl"
	sudo apt install curl -y
fi

if [ -z "$(command -v tmux)" ]; then
	printInstallMessage "tmux"
	sudo apt install tmux -y
fi

if [ -z "$(command -v ranger)" ]; then
	printInstallMessage "ranger"
	./01_File_Manager/ranger_install.sh
fi

if [ -z "$(command -v rg)" ]; then
	printInstallMessage "ripgrep"
	./ripgrep_install.sh
fi

if [ -z "$(command -v fd)" ]; then
	printInstallMessage "fd"
	./fd_install.sh
fi

if [ -z "$(command -v fzf)" ]; then
	printInstallMessage "fzf"
	./fzf_install.sh
fi
