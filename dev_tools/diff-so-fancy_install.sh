#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

DIFF_SO_FANCY_REPO="https://github.com/so-fancy/diff-so-fancy"
DIFF_SO_FANCY_DIR="${PROGRAM_PATH}/diff-so-fancy"
GIT_PATH=`command -v git`
BIN_DIR=${HOME}/.bin

if ! [ -e ${GIT_PATH} ]; then
	echo "Need to install git!!!"
	exit
fi

if ! [ -d ${DIFF_SO_FANCY_DIR} ]; then
	echo "Install diff-so-fancy!!"
	git clone --depth=1 ${DIFF_SO_FANCY_REPO} ${DIFF_SO_FANCY_DIR}
	ln -fs ${DIFF_SO_FANCY_DIR}/diff-so-fancy ${BIN_DIR}/diff-so-fancy
else
	echo "Update diff-so-fancy!!"
	cd ${DIFF_SO_FANCY_DIR}
	git pull
	ln -fs ${DIFF_SO_FANCY_DIR}/diff-so-fancy ${BIN_DIR}/diff-so-fancy
	exit
fi
