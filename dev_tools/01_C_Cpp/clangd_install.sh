#!/bin/bash

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

if [ $UBUNTU_CODENAME == focal ]; then
	sudo apt install clangd-12 -y
	sleep 1
	echo "change default clangd"
	sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-12 12
elif [ $UBUNTU_CODENAME == xenial ]; then
	sudo apt install clang-tools-8 -y
	sleep 1
	echo "change default clangd"
	sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-8 8 
fi
