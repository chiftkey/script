#!/bin/bash

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

TARGET_DIR=${PROGRAM_PATH}/ctags

# https://github.com/universal-ctags/ctags.git

sudo apt install build-essential automake pkg-config -y

rm -rf ${TARGET_DIR}
git clone --depth 1 https://github.com/universal-ctags/ctags.git ./tmp
sleep 1
cd ./tmp && ./autogen.sh && ./configure --prefix=${TARGET_DIR}
make && make install
cd .. && rm -rf ./tmp

# Make symbolic link
ln -sf ${TARGET_DIR}/bin/ctags ${USER_BIN_DIR}/ctags
