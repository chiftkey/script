#!/bin/sh

# https://github.com/wfxr/code-minimap/releases/tag/v0.6.1
# https://github.com/wfxr/code-minimap/releases/download/v0.6.1/code-minimap_0.6.1_amd64.deb

REPONAME="wfxr/code-minimap"
PRGNAME="code-minimap"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME="${PRGNAME}_${VERSION_NAME}_amd64.deb"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# install package
sudo apt install ./${FILENAME}
# delete files
cd .. && rm -rf ./tmp


# extract source
# tar xf ${FILENAME}
# install
# sudo ./install.sh
