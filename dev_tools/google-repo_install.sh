#!/bin/sh

if ! [ -d ${HOME}/.bin ]; then
	mkdir -p ${HOME}/.bin
fi

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo \
&& chmod a+x ~/.bin/repo
