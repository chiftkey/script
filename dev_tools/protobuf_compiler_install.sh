#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# https://github.com/protocolbuffers/protobuf/releases/download/v3.19.1/protoc-3.19.1-linux-x86_64.zip

REPONAME="protocolbuffers/protobuf"
# PRGNAME=`echo ${REPONAME} | cut --delimiter='/' -f 2`
PRGNAME="protoc"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="zip"
FILENAME="${PRGNAME}-${VERSION_NAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH}.${FILENAME_EXTENSION}"
echo "FileName = ${FILENAME}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
unzip ${FILENAME}
# make file executable
chmod a+x ./bin/${PRGNAME}
# install
install ./bin/${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp
