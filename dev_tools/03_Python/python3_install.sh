#!/bin/sh

if [ -z "$(command -v python3)" ]; then
	sudo apt install python3 -y
fi

if [ -z "$(command -v pip3)" ]; then
	sudo apt install python3-pip -y
fi

if [ -z "$(dpkg --get-selections python3-venv)" ]; then
	sudo apt install python3-venv -y
fi

if [ -z "$(pip3 list | grep pynvim)" ]; then
	pip3 install pynvim
fi
