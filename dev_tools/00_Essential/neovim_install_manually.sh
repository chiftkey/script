#!/bin/sh

#https://github.com/neovim/neovim/releases/download/stable/nvim.appimage

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

REPONAME="neovim/neovim"
PRGNAME="nvim"
#VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
VERSION="stable"
#echo "Latest version = $VERSION"
#VERSION_NAME=`echo ${VERSION} | cut -c 2- `
#FILENAME="${PRGNAME}-${VERSION_NAME}-linux-x86-64.tbz"
FILENAME="${PRGNAME}.appimage"
EXTRACTED_APP_IMG_DIR="squashfs-root"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}

if [ -e $(command -v nvim) ]; then
	sudo apt purge neovim -y
	rm -rf ${USER_BIN_DIR}/nvim
	rm -rf ${USER_BIN_DIR}/nvim
	rm -rf ${USER_BIN_DIR}/AppImage/${FILENAME}
fi

# To fix the issue "Unable to locate theme engine in module_path"
sudo apt install gnome-themes-standard -y
# To use vim-mundo plugin
pip3 install pynvim

TARGET_DIR=${PROGRAM_PATH}/neovim
if ! [ -d ${TARGET_DIR} ]; then
	mkdir -p ${TARGET_DIR}
else
	rm -rf ${TARGET_DIR}/*
fi

echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1

# make file executable
chmod u+x ${FILENAME}
# extract appimage file
./${FILENAME} --appimage-extract
# move result to the user home bin_dir
cp -rlf ${EXTRACTED_APP_IMG_DIR}/* ${TARGET_DIR}
# make symbolic link
ln -snf ${TARGET_DIR}/AppRun ${USER_LOCAL_BIN_DIR}/nvim
# copy app
cp -rf ${TARGET_DIR}/usr/share/applications/. ${HOME}/.local/share/applications/.
# delete files
cd .. && rm -rf ./tmp
