#!/bin/sh

JDK_NAME="adoptopenjdk-11-openj9"

if ! [ -e $(command -v java) ]; then
	exit 1
fi

wget https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public

gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --import public

gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --export --output adoptopenjdk-archive-keyring.gpg

rm adoptopenjdk-keyring.gpg*

sudo mv adoptopenjdk-archive-keyring.gpg /usr/share/keyrings

CODENAME=`cat /etc/os-release | grep UBUNTU_CODENAME | cut -d = -f 2`

echo "deb [signed-by=/usr/share/keyrings/adoptopenjdk-archive-keyring.gpg] https://adoptopenjdk.jfrog.io/adoptopenjdk/deb ${CODENAME} main" | sudo tee /etc/apt/sources.list.d/adoptopenjdk.list

sudo apt update -y && sudo apt install ${JDK_NAME} -y

rm public
