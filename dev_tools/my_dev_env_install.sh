#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

printInstallMessage "Python relates"
./03_Python/python3_install.sh
./03_Python/make_python3_to_default_python.sh
sleep 1

printInstallMessage "Essential relates"
./00_Essential/prerequisite_install.sh
./00_Essential/neovim_install_manually.sh
./04_Git/lazygit_install_manually.sh
sleep 1

printInstallMessage "C & Cpp relates"
./01_C_Cpp/astyle_install.sh
./01_C_Cpp/clangd_install.sh
./01_C_Cpp/ctags_install.sh
sleep 1

printInstallMessage "Etc"
./nodejs_lts_install.sh
