#!/bin/sh

# Text-mode interface for git

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# https://github.com/jonas/tig/releases/download/tig-2.5.4/tig-2.5.4.tar.gz

REPONAME="jonas/tig"
PRGNAME=`echo ${REPONAME}| cut --delimiter='/' -f 2`
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = [$VERSION]"
# VERSION_NAME=`echo ${VERSION} | cut -c 2- `
# FILENAME_SUFFIX="musl"
FILENAME_EXTENSION="tar.gz"
# FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
FILENAME="${VERSION}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
INSTALL_DIR=${PROGRAM_PATH}/tig
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
tar xf ${FILENAME}

if [ -z "$(command -v make)" ]; then
	sudo apt install make -y
fi

sudo apt install libncurses5-dev libncursesw5-dev -y

# install
cd ${VERSION}
make prefix=${INSTALL_DIR} -j -s
make install prefix=${INSTALL_DIR} -j

# make symbolic link
ln -svf ${INSTALL_DIR}/bin/${PRGNAME} ${USER_LOCAL_BIN_DIR}/${PRGNAME}

# delete files
cd ../../ && rm -rf ./tmp
