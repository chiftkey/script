#!/bin/sh

# Blazing 💥 fast terminal-ui for git written in rust 🦀

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# https://github.com/extrawurst/gitui/releases/download/v0.18.0/gitui-linux-musl.tar.gz

REPONAME="extrawurst/gitui"
PRGNAME=`echo ${REPONAME}| cut --delimiter='/' -f 2`
# PRGNAME="protoc"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_SUFFIX="musl"
FILENAME_EXTENSION="tar.gz"
FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${FILENAME_SUFFIX}.${FILENAME_EXTENSION}"
echo "FileName = ${FILENAME}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract source
tar xf ${FILENAME}
# make file executable
chmod a+x ${PRGNAME}
# install
install ${PRGNAME} ${USER_BIN_DIR}/${PRGNAME}
# delete files
cd .. && rm -rf ./tmp
