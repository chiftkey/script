#!/bin/sh

#https://github.com/jesseduffield/lazygit/releases/download/v0.30.1/lazygit_0.30.1_Linux_x86_64.tar.gz

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# if [ -e $(command -v lazygit) ]; then
	# sudo apt install ppa-purge -y
	# sudo ppa-purge lazygit-team/release --force-yes
	# sudo apt purge lazygit -y
# fi

REPONAME="jesseduffield/lazygit"
PRGNAME="lazygit"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
# make like this : v0.30.1 -> 0.30.1
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="tar.gz"
FILENAME="${PRGNAME}_${VERSION_NAME}_${KERNEL_NAME}_${SYSTEM_ARCH}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# extract bin
tar zxf ${FILENAME}
# install binary
install -m 755 -v ${PRGNAME} ${USER_LOCAL_BIN_DIR}/

# delete files
cd .. && rm -rf ./tmp
