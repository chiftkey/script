#!/bin/sh

sudo apt install software-properties-common -y

sudo add-apt-repository ppa:lazygit-team/release -y \
	&& sudo apt update -y \
	&& sudo apt install lazygit -y
