#!/bin/sh

PROGRAM_PATH="/usr/share/applications/appimagelauncher.desktop"

if [ -e ${PROGRAM_PATH} ]; then
	exit 1
fi

sudo add-apt-repository ppa:appimagelauncher-team/stable \
&& sudo apt update \
&& sudo apt install appimagelauncher
