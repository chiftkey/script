#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

REPONAME="ksnip/ksnip"
PRGNAME="ksnip"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`

echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="deb"
FILENAME="${PRGNAME}-${VERSION_NAME}.${FILENAME_EXTENSION}"
# FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH2}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

#===========================Install====================================================================================
# install dependencies
sudo apt install libqt5x11extras5 -y

# download package
mkdir -p ./tmp
cd ./tmp
curl -LO ${DOWNLOAD_URL}
sleep 1
# install downloaded package
sudo apt install ./${FILENAME}
# delete temp files
cd .. && rm -rf ./tmp
