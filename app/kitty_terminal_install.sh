#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

# https://github.com/kovidgoyal/kitty/releases/download/v0.24.2/kitty-0.24.2-x86_64.txz
REPONAME="kovidgoyal/kitty"
PRGNAME="kitty"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
APP_PATH="${PROGRAM_PATH}/kitty.app"

echo "Latest version = $VERSION"
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME_EXTENSION="txz"
FILENAME="${PRGNAME}-${VERSION_NAME}-${SYSTEM_ARCH}.${FILENAME_EXTENSION}"
# FILENAME="${PRGNAME}-${KERNEL_NAME_LOWERCASE}-${SYSTEM_ARCH2}.${FILENAME_EXTENSION}"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}

# download package
echo "${DOWNLOAD_URL}"
mkdir -p ${TEMP_DIR}
cd ${TEMP_DIR}
curl -LO ${DOWNLOAD_URL}
sleep 1
mkdir -p ${APP_PATH}
tar -C ${APP_PATH} -xf ${FILENAME}

#===============================================================================================================================================
# Refer to : https://sw.kovidgoyal.net/kitty/binary/#desktop-integration-on-linux
#===============================================================================================================================================
# Create a symbolic link to add kitty to PATH (assuming ~/.local/bin is in your PATH)
ln -snf ${APP_PATH}/bin/kitty ${USER_LOCAL_BIN_DIR}/ # Place the kitty.desktop file somewhere it can be found by the OS
cp ${APP_PATH}/share/applications/kitty.desktop ~/.local/share/applications/
# Update the path to the kitty icon in the kitty.desktop file
# sed -i "s|Icon=kitty|Icon=$APP_PATH/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty.desktop
# delete download file
rm -rf ${TEMP_DIR}
