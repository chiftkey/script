#!/bin/sh

#include
ROOT_PATH=$(git rev-parse --show-toplevel)
. ${ROOT_PATH}/include/env.sh

CUSTOM_SCRIPT_DIR="${PROGRAM_PATH}/custom"

if [ ! -d "${CUSTOM_SCRIPT_DIR}" ]; then
	mkdir -p ${CUSTOM_SCRIPT_DIR}
fi

cp $(pwd)/mktags.sh ${CUSTOM_SCRIPT_DIR}/.

ln -sf ${CUSTOM_SCRIPT_DIR}/mktags.sh ${USER_BIN_DIR}/mktags
