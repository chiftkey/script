#!/bin/sh

CTAG_FILE="tags"
SRC_FILES="file_list"

find ./ -name '*.[cCsShH]' > ${SRC_FILES}

echo "make ctags"
ctags -L ${SRC_FILES} -o ${CTAG_FILE} --c++-kinds=+p --fields=+iaS --extras=+q

echo "make cscope"
cscope -b -i ${SRC_FILES}

rm -rf ${SRC_FILES}
