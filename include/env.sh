#!/bin/sh

# ex) Linux
KERNEL_NAME=$(uname -s)

# ex) Linux -> linux
KERNEL_NAME_LOWERCASE=`echo $(uname -s) | awk '{print tolower($0)}'`

# ex) aarch64, x86_64
SYSTEM_ARCH=$(uname -m)

if [ -e $(command -v dpkg) ]; then
	SYSTEM_ARCH2=$(dpkg --print-architecture)
fi

# ex) zsh, bash
SHELL_NAME=`echo $SHELL | rev | cut -d'/' -f 1 | rev`

PROGRAM_PATH=${HOME}/.local/extras
if [ ! -d "${PROGRAM_PATH}" ];then
	mkdir -p ${PROGRAM_PATH}
fi

USER_BIN_DIR=${HOME}/.bin
if [ ! -d "${USER_BIN_DIR}" ];then
	mkdir -p ${USER_BIN_DIR}
fi

USER_LOCAL_BIN_DIR=${HOME}/.local/bin
if [ ! -d "${USER_LOCAL_BIN_DIR}" ];then
	mkdir -p ${USER_LOCAL_BIN_DIR}
fi

UBUNTU_CODENAME=`cat /etc/os-release | grep UBUNTU_CODENAME | awk -F'=' '{ print $2}'`

TEMP_DIR=$(pwd)/tmp

printInstallMessage()
{
	cat <<- EOF

	========================================

	Install $1 !!!!!!!

	========================================

	EOF
}
