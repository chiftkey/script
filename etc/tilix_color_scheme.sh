#!/bin/sh

TILIX_SCHEME_DIR=${HOME}/.config/tilix/schemes

if [ ! -d ${TILIX_SCHEME_DIR} ];then
	mkdir -p ${TILIX_SCHEME_DIR}
fi

git clone --depth=1 https://github.com/storm119/Tilix-Themes ./tmp

cd ./tmp

#mv -f */*.json ${TILIX_SCHEME_DIR}
mv -f */thayer-bright.json ${TILIX_SCHEME_DIR}

cd .. && rm -rf ./tmp
