#!/bin/sh

sudo apt install gnome-shell -y

git clone --depth=1 https://github.com/vinceliuice/vimix-gtk-themes ./tmp/vimix-themes
git clone --depth=1 https://github.com/vinceliuice/vimix-icon-theme ./tmp/vimix-icon

./tmp/vimix-themes/install.sh
./tmp/vimix-icon/install.sh

rm -rf ./tmp
