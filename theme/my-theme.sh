#!/bin/sh

echo "Prerequisite Package Install"
sudo apt install sassc -y

echo "Install Papirus Icon Theme"
./papirus_icon_install.sh

echo "Install Orchis-theme"
./orchis_theme_install.sh

echo "Install Vimix-theme"
./vimix_theme_install.sh
