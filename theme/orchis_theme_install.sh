#!/bin/sh

git clone --depth=1 https://github.com/vinceliuice/Orchis-theme ./temp/Orchis-theme
git clone --depth=1 https://github.com/vinceliuice/Tela-circle-icon-theme ./temp/Tela-circle-icon-theme
git clone --depth=1 https://github.com/vinceliuice/Tela-icon-theme ./temp/Tela-icon-theme
cd ./temp
./Orchis-theme/install.sh
./Tela-circle-icon-theme/install.sh
./Tela-icon-theme/install.sh
cd .. && rm -rf ./temp
