#!/bin/bash

CONTAINER_LIST=`docker ps --format '{{.Names}}' | grep -v mariadb`
CONTAINER_LIST_STRINGS=($CONTAINER_LIST)
MARIA_DB_CONTAINERS=`docker ps --format '{{.Names}}' | grep -iE 'mariadb*'`
MARIA_DB_CONTAINERS_STRING=($MARIA_DB_CONTAINERS)

help_print()
{
cat <<EOF

########################################
mariadb update
$0 1
container update
$0 2
########################################

EOF
	return 0;
}

if [ $# -eq 0 ]; then
	help_print
	exit 0;
fi

if [ ${1} -eq "1" ]; then
	docker run --rm \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-e TZ=Asia/Seoul containrrr/watchtower:latest \
		--run-once ${MARIA_DB_CONTAINERS_STRING[@]}
fi

if [ ${1} -eq "2" ]; then
	docker run --rm \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-e TZ=Asia/Seoul containrrr/watchtower:latest \
		--run-once ${CONTAINER_LIST_STRINGS[@]}
fi
