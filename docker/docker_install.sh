#!/bin/sh

if [ -n "$(command -v docker)" ]; then
	echo "Docker has already installed"
	exit
fi

cat << EOM
==============================
Remove old version docker engine
==============================
EOM

sudo apt purge docker docker-engine docker.io containerd runc

cat << EOM
==============================
Install Prerequisite package
==============================
EOM

sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

cat << EOM
==============================
Add Docker’s official GPG key
==============================
EOM

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

cat << EOM
==============================
Add Docker stable repository
==============================
EOM

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

cat << EOM
==============================
Install docker-ce engine
==============================
EOM

sudo apt update -y && \
sudo apt install docker-ce docker-ce-cli containerd.io -y
