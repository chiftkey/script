#!/bin/sh

# Refer to
# https://docs.docker.com/compose/install/

REPONAME="docker/compose"
PRGNAME="docker-compose"
VERSION=`curl --silent "https://api.github.com/repos/${REPONAME}/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = ${VERSION}"

INSTALL_DIR="${HOME}/.bin"

# make directory if not exist
if ! [ -d ${INSTALL_DIR} ]; then
	mkdir -p ${INSTALL_DIR}
fi

# download & install
curl -L "https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-$(uname -s)-$(uname -m)" \
	-o ${INSTALL_DIR}/${PRGNAME}

# set executable
echo "set executable"
chmod a+x ${INSTALL_DIR}/${PRGNAME}
