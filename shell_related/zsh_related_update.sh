#!/bin/zsh

echo "Update [fast-syntax-highlighting]"
cd ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/fast-syntax-highlighting \
&& git pull
sleep 1

echo "Update [zsh-autosuggestions]"
cd ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions \
&& git pull
sleep 1

echo "Update [powerlevel10k]"
cd ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k \
&& git pull
sleep 1

echo "Update [fzf]"
cd ~/.fzf && git pull \
	&& ./install --completion --key-bindings --no-update-rc --no-fish \
	&& cd -
sleep 1

source ${HOME}/.zshrc
echo "Update [Oh-my-zsh]"
omz update
sleep 1

echo "FINISH!!"
exit 0
