#!/bin/sh

OMZ_DIR="${HOME}/.oh-my-zsh"
OMZ_CUSTOM_DIR="${ZSH_CUSTOM:-${OMZ_DIR}/custom}"

cat << EOM
==============================
Install Prerequisite packages
==============================
EOM
sudo apt install zsh curl git -y

if ! [ -d ${OMZ_DIR} ]; then
cat << EOM
==============================
Install oh-my-zsh
==============================
EOM
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi
