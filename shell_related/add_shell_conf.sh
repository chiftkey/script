#!/bin/sh

SHELL_CONF=~/.`echo $SHELL | rev | cut -d'/' -f 1 | rev`rc
SHELL_CUSTOM_FILE="shrc.custom"
TARGET_PATH=${HOME}/.${SHELL_CUSTOM_FILE}

if ! [ -e  ${TARGET_PATH} ]; then
	echo "no ${SHELL_CUSTOM_FILE}!!"
	cp ${SHELL_CUSTOM_FILE} ${TARGET_PATH}
fi

echo ". ${TARGET_PATH}" >> $SHELL_CONF
