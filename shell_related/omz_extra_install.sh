#!/bin/sh

OMZ_DIR="${HOME}/.oh-my-zsh"
OMZ_CUSTOM_DIR="${ZSH_CUSTOM:-${OMZ_DIR}/custom}"
FAST_SYNTAX_DIR="${OMZ_CUSTOM_DIR}/plugins/fast-syntax-highlighting"


if [ ! -d ${FAST_SYNTAX_DIR} ]; then
cat << EOM
==============================
Install fast-syntax-highlighting
==============================
EOM
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git ${OMZ_CUSTOM_DIR}/plugins/fast-syntax-highlighting
else
	rm -rf ${FAST_SYNTAX_DIR}
	git clone https://github.com/zdharma-continuum/fast-syntax-highlighting.git ${OMZ_CUSTOM_DIR}/plugins/fast-syntax-highlighting
fi

if ! [ -d ${OMZ_CUSTOM_DIR}/plugins/zsh-autosuggestions ]; then
cat << EOM
==============================
Install zsh-autosuggestions
==============================
EOM
git clone https://github.com/zsh-users/zsh-autosuggestions ${OMZ_CUSTOM_DIR}/plugins/zsh-autosuggestions
fi

if ! [ -d ${OMZ_CUSTOM_DIR}/themes/powerlevel10k ]; then
cat << EOM
==============================
Install powerlevel10k
==============================
EOM
git clone https://github.com/romkatv/powerlevel10k.git ${OMZ_CUSTOM_DIR}/themes/powerlevel10k
fi
