#!/bin/bash

if [ -z "$(command -v curl)" ]; then
	sudo apt install curl -y
fi

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)"
