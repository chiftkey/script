#!/bin/sh

#https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip
FONT_NAME="JetBrainsMono-NerdFont"
FONT_PATH=${HOME}/.local/share/fonts/truetype/${FONT_NAME}

REPONAME="ryanoasis/nerd-fonts"
#PRGNAME="fd"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
echo "Latest version = $VERSION"
#VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FILENAME="JetBrainsMono.zip"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${FILENAME}
echo "${DOWNLOAD_URL}"

# download package
echo "download font"
curl -LO "${DOWNLOAD_URL}"
# install downloaded package
unzip ${FILENAME} -d temp
echo "move font"
mkdir -p ${FONT_PATH} && mv ./temp/*.ttf ${FONT_PATH}
fc-cache
# delete temp file
rm -rf ./temp
rm -rf ${FILENAME}
