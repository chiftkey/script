#!/bin/sh

REPONAME="spoqa/spoqa-han-sans"
FONTNAME="SpoqaHanSansNeo"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
FONT_PATH=${HOME}/.local/share/fonts/opentype/${FONTNAME}
TEMP_DIR=$(pwd)/tmp
OTF_FILENAME="${FONTNAME}_OTF_original.zip"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${OTF_FILENAME}

mkdir ${TEMP_DIR} && cd ${TEMP_DIR}
echo "Download font"
# https://github.com/spoqa/spoqa-han-sans/releases/download/v3.3.0/SpoqaHanSansNeo_OTF_original.zip
curl -LO ${DOWNLOAD_URL}
unzip -j ${OTF_FILENAME}
rm -rf ${OTF_FILENAME}

echo "Copy font"
mkdir -p ${FONT_PATH} && cp -rf ${TEMP_DIR}/. ${FONT_PATH}/.
echo "fc-cache"
fc-cache

rm -rf ${TEMP_DIR}
