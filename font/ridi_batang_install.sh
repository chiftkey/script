#!/bin/sh

FONT_PATH=${HOME}/.local/share/fonts/opentype/ridi/

echo "download font"
curl https://www.ridicorp.com/wp-content/themes/ridicorp/css/font/RIDIBatang.otf -o /tmp/RIDIBatang.otf
echo "move font"
mkdir -p ${FONT_PATH} && cp /tmp/RIDIBatang.otf ${FONT_PATH}
echo "fc-cache"
fc-cache
