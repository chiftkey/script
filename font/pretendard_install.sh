#!/bin/sh

# https://github.com/orioncactus/pretendard/releases/download/v1.2.1/Pretendard-1.2.1.zip

REPONAME="orioncactus/pretendard"
FONTNAME="Pretendard"
VERSION=`curl --silent "https://api.github.com/repos/$REPONAME/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
VERSION_NAME=`echo ${VERSION} | cut -c 2- `
FONT_PATH=${HOME}/.local/share/fonts/opentype/${FONTNAME}
TEMP_DIR=$(pwd)/tmp
OTF_FILENAME="${FONTNAME}-${VERSION_NAME}.zip"
DOWNLOAD_URL=https://github.com/${REPONAME}/releases/download/${VERSION}/${OTF_FILENAME}

mkdir ${TEMP_DIR} && cd ${TEMP_DIR}
echo "Download font"
# https://github.com/spoqa/spoqa-han-sans/releases/download/v3.3.0/SpoqaHanSansNeo_OTF_original.zip
curl -LO ${DOWNLOAD_URL}
unzip -j ${OTF_FILENAME}
rm -rf ${OTF_FILENAME}
rm -rf *.ttf *.woff* *.css

echo "Copy font"
mkdir -p ${FONT_PATH} && cp -rf ${TEMP_DIR}/. ${FONT_PATH}/.
echo "fc-cache"
fc-cache

rm -rf ${TEMP_DIR}
